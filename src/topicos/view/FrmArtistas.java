/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package topicos.view;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import topicos.model.ConexionBD;

/**
 *
 * @author Laboratorio
 */
public class FrmArtistas extends javax.swing.JFrame {

    DefaultTableModel modeloTabla;
    DefaultComboBoxModel modeloCombo;
    ConexionBD bd;
    int selectedRow=-1;
    int selectedRowDisquera=-1;
    String selectedId ;
    String selectedDisqueraId ;
    
    public void agregarArtista(String nombre, int idDisquera){
        
        String stmtSQL = "insert into artistas (nombre,Disqueras_idDisquera) values (?,?);";
       
        PreparedStatement pStmt = null; 
               
        try {
            pStmt = bd.conn.prepareStatement(stmtSQL);
            
            pStmt.setString(1, nombre);
            pStmt.setInt(2, idDisquera);
            
            if ( pStmt.execute() ){
               JOptionPane.showMessageDialog(this, "Error en insercion de registro");
            }} catch (SQLException ex) {
            Logger.getLogger(FrmArtistas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void actualizarArtista(String selectedId, String nombre) {
       String stmtSQL = "update Disqueras set nombre = '"+nombre+"' where idDisquera = "+selectedId+";";
               
        try {
            if ( bd.stat.execute(stmtSQL) ){
               JOptionPane.showMessageDialog(this, "Error en actualizacion de registro");
            }} catch (SQLException ex) {
            Logger.getLogger(FrmArtistas.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.selectedRow = -1;
        this.cargarArtistas();
    }    

    private void eliminarArtista(String selectedId) {
       String stmtSQL = "delete from Disqueras where idDisquera = "+selectedId+";";
               
        try {
            if ( bd.stat.execute(stmtSQL) ){
               JOptionPane.showMessageDialog(this, "Error en eliminacion de registro");
            } else {
                this.selectedRow = -1;
                this.cargarArtistas();                
            }
        } catch (SQLException ex) {
            Logger.getLogger(FrmArtistas.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void cargarArtistas(){
        modeloTabla = (DefaultTableModel) this.jTable1.getModel();
        
        modeloTabla.setRowCount(0);  // Eliminamos toda la informacion del JTable
        try {
        
            this.bd.res =  bd.stat.executeQuery("select a.idArtista, d.nombre as nombreDisquera,a.nombre from artistas a inner join disqueras d on a.Disqueras_idDisquera = d.idDisquera");
            
            while (bd.res.next()){
                int idArtista = bd.res.getInt("idArtista");
                String disquera = bd.res.getString("nombreDisquera");
                String nombre = bd.res.getString("nombre");
                
                Object[] obj = {idArtista, disquera, nombre};
                
                modeloTabla.addRow(obj);
                              
            }                                    
            
        } catch (SQLException ex) {
            Logger.getLogger(FrmArtistas.class.getName()).log(Level.SEVERE, null, ex);
        
        }
    }
    
    private void cargarDisqueras() {
        modeloCombo = (DefaultComboBoxModel) this.cbDisqueras.getModel();
        
        modeloCombo.removeAllElements(); // Eliminamos toda la informacion del Combo
        try {
        
            this.bd.res =  bd.stat.executeQuery("select d.idDisquera, d.nombre from disqueras d order by idDisquera");
            
            while (bd.res.next()){
                int idDisquera = bd.res.getInt("idDisquera");                
                String nombre = bd.res.getString("nombre");
                                             
                modeloCombo.addElement(nombre);
            }                                    
            
        } catch (SQLException ex) {
            Logger.getLogger(FrmArtistas.class.getName()).log(Level.SEVERE, null, ex);
        
        }        
    }    
    
    public void filtrarArtistas(String _nombre){
        modeloTabla = (DefaultTableModel) this.jTable1.getModel();
        
        modeloTabla.setRowCount(0);  // Eliminamos toda la informacion del JTable
        try {
        
            this.bd.res =  bd.stat.executeQuery("select idDisquera, nombre from Disqueras where nombre like '%"+_nombre.trim()+"%'");
            
            while (bd.res.next()){
                int idDisquera = bd.res.getInt("idDisquera");
                String nombre = bd.res.getString("nombre");
                
                Object[] obj = {idDisquera, nombre};
                
                modeloTabla.addRow(obj);
                              
            }                                    
            
        } catch (SQLException ex) {
            Logger.getLogger(FrmArtistas.class.getName()).log(Level.SEVERE, null, ex);
        
        }
    }
    
    /**
     * Creates new form FrmDisqueras
     */
    public FrmArtistas() {
        this.bd = new ConexionBD();   
        this.bd.conectar();
        initComponents();
        this.cargarArtistas();
        
        this.cargarDisqueras();
        
        this.jTable1.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
        public void valueChanged(ListSelectionEvent event) {
            selectedRow = jTable1.getSelectedRow();
            
            if (selectedRow>=0){
                selectedId = jTable1.getValueAt(selectedRow, 0).toString();
                jTextField1.setText(jTable1.getValueAt(selectedRow, 1).toString());            
            }
        }
    });
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jToolBar1 = new javax.swing.JToolBar();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        cbDisqueras = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jLabel1.setText("Nombre del Artista");

        jToolBar1.setFloatable(false);

        jButton1.setText("Filtrar");
        jButton1.setFocusable(false);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        jButton2.setText("Agregar");
        jButton2.setFocusable(false);
        jButton2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton2);

        jButton3.setText("Guardar");
        jButton3.setFocusable(false);
        jButton3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton3.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton3);

        jButton4.setText("Eliminar");
        jButton4.setFocusable(false);
        jButton4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton4.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton4);

        jButton5.setText("Actualizar");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton5);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Disquera", "Nombre"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setColumnSelectionAllowed(true);
        jScrollPane1.setViewportView(jTable1);
        jTable1.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(10);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
        }

        jLabel2.setText("Disquera");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 642, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextField1)
                            .addComponent(cbDisqueras, 0, 328, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbDisqueras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(13, 13, 13)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 356, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        this.cargarArtistas();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        if (!this.jTextField1.getText().isEmpty()){
            this.agregarArtista(this.jTextField1.getText(),1);    
            this.cargarArtistas();
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        if (this.jTable1.getSelectedRow()>=0){
            this.actualizarArtista(this.selectedId,this.jTextField1.getText());
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un registro");
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        if (this.jTable1.getSelectedRow()>=0){
            this.eliminarArtista(this.selectedId);
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un registro");
        }     
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        this.filtrarArtistas(this.jTextField1.getText()); 
        
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> cbDisqueras;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JToolBar jToolBar1;
    // End of variables declaration//GEN-END:variables
}
